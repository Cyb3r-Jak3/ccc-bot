### What does this MR accomplish

### Related issues

### Tests

#### Cogs

* [ ] AdminCog
* [ ] EventCog
* [ ] HealthCog
* [ ] MiscCog
* [ ] RankCog
* [ ] RegionCog
* [ ] SchoolCog
* [ ] SearchCog
* [ ] TaskCog

#### Database

* [ ] Tables test

### Changelog

* [ ] Changelog updated
