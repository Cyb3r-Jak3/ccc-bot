<!-- markdownlint-disable MD001 -->

# CCC-Bot

### This bot is still in beta stages

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/Cyb3r-Jak3/ccc-bot?style=flat-square)](https://gitlab.com/Cyb3r-Jak3/ccc-bot/builds)

[![Maintainability](https://api.codeclimate.com/v1/badges/027b6135ce0965aa69c1/maintainability)](https://codeclimate.com/github/Cyb3r-Jak3/CCC-Bot/maintainability) [![Code Climate issues](https://img.shields.io/codeclimate/issues/Cyb3r-Jak3/CCC-Bot?style=flat-square)](https://codeclimate.com/github/Cyb3r-Jak3/CCC-Bot/issues)

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/Cyb3r-Jak3/CCC-Bot/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/Cyb3r-Jak3/CCC-Bot/?branch=master)

## About

This is a bot for the Competitive Cyber Club Discord. It is made specifically for it, so it has
    been highly tailored to meet the unique requirements of it.
As such it is not recommended for other discord servers, however it is
    something that you should be able to learn from to create your own.

Written for Python 3.5+

### To add

Please visit this [link](https://discordapp.com/api/oauth2/authorize?client_id=643200662045458444&permissions=268960950&scope=bot)

[![DeepSource](https://static.deepsource.io/deepsource-badge-light-mini.svg)](https://deepsource.io/gl/Cyb3r-Jak3/CCC-Bot/?ref=repository-badge)
